﻿using UnityEngine;
using System.Collections;

public class BoxState : MonoBehaviour {

	[SerializeField] float boxDropDistance;

	GameCtrl gameCtrl;
	bool isNowCarry = true;
	Transform playerTrans;
	float score;
	int boxIndex;

	// Use this for initialization
	void Start () {
		gameCtrl = GameObject.Find ("GameCtrl").GetComponent<GameCtrl> ();
		playerTrans = GameObject.FindGameObjectWithTag ("Player").transform;
		score = Vector3.Distance (gameCtrl.getDeparturePoint(), gameCtrl.getArrivalPoint());
	}
	
	// Update is called once per frame
	void Update () {

		if (Vector3.Distance (transform.position, playerTrans.position) >= boxDropDistance
			&& isNowCarry) {
			GameCtrl.BoxNum--;
			isNowCarry = false;
			if(GameCtrl.BoxNum == 0){
				gameCtrl.setPoint();
				gameCtrl.resetBoxScore();
			}
		}

		if (isNowCarry) {
			score -= Time.deltaTime;
			if(score <= 0){
				score = 0;
			}		
			gameCtrl.setBoxScore (score);
		}

		//Debug.Log (score);
	}

	bool isCarry(){
		return isNowCarry;
	}

	void OnTriggerEnter(Collider _other){

		if (_other.tag == "Respawn") {
			BoxPoint point = _other.gameObject.GetComponent<BoxPoint>();
			switch(point.getPointRole()){
			case BoxPoint.PointRole.Departure:
				break;
			case BoxPoint.PointRole.Arrival:
				GameCtrl.ArrivalBoxNum++;
				if(GameCtrl.BoxNum == GameCtrl.ArrivalBoxNum){
					point.setPointRole(BoxPoint.PointRole.Neutral);
					gameCtrl.boxArrival();
					gameCtrl.setPoint();	
					gameCtrl.resetBoxScore();
				}
				Destroy(gameObject);
				break;
			}
		}

	}

}
