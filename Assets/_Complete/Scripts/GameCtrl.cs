using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public class GameCtrl : MonoBehaviour {

	[SerializeField] BoxPoint[] boxPoint = new BoxPoint[6];
	[SerializeField] UICtrl uiCtrl;
	[SerializeField] float gameTime = 180f; 
	float allscore = 0;
	float boxScore = 0;
	int departureNum;
	int arrivalNum;
	bool isFinish = false;	

	private static int boxNum = 0;
	public static int BoxNum {
		get{return boxNum;}
		set{boxNum = value;}
	}

	private static int arrivalBoxNum = 0;
	public static int ArrivalBoxNum{
		get{return arrivalBoxNum;}
		set{arrivalBoxNum = value;}
	}

	// Use this for initialization
	void Start () {

		setPoint ();
		boxNum = 0;

	}
	
	// Update is called once per frame
	void Update () {

		gameTime -= Time.deltaTime;
		if (gameTime <= 0) {
			gameTime = 0;
			isFinish = true;
		}
		uiCtrl.setTime (gameTime);
		if (isFinish) {
			uiCtrl.setUIFinish();
			if(PlayerPrefs.GetInt("HighScore") < allscore){
				PlayerPrefs.SetInt("HighScore",(int)allscore);
				Analytics.CustomEvent("GameFinish",new Dictionary<string,object>{{"Score",(int)allscore}});
			}
			GameObject.Find("FadeManager").GetComponent<FadeManager>().LoadLevel("Title",3.01f);
		}
	}

	public void setPoint(){

		for (int i = 0; i < boxPoint.Length; i++) {
			boxPoint[i].setPointRole(BoxPoint.PointRole.Neutral);
		}
		departureNum = (int)Random.Range (0, boxPoint.Length);
		arrivalNum = (int)Random.Range (0, boxPoint.Length);
		while (arrivalNum == departureNum) {
			arrivalNum = (int)Random.Range(0,boxPoint.Length);
		}
		boxPoint [departureNum].setPointRole (BoxPoint.PointRole.Departure);
		boxPoint [arrivalNum].setPointRole (BoxPoint.PointRole.Arrival);
		//Debug.Log ("Start:" + departureNum + " Goal:" + arrivalNum);
		boxNum = 0;	
		ArrivalBoxNum = 0;
	}

	public Vector3 getDeparturePoint(){
		return boxPoint [departureNum].transform.position;
	}

	public Vector3 getArrivalPoint(){
		return boxPoint [arrivalNum].transform.position;
	}

	public void setBoxScore(float score){
		boxScore = score;
		uiCtrl.setBoxPoint ((int)boxScore * (boxNum + 1));
		//Debug.Log (score + ":" + boxScore);
	}

	public void boxArrival(){
		allscore += boxScore * (boxNum + 1);
		boxScore = 0;
		uiCtrl.setAllPoint ((int)allscore);
	}


	public void resetBoxScore(){
		boxScore = 0;
		uiCtrl.setBoxPoint ((int)boxScore * (boxNum + 1));
	}

	public void resetPlayerPosition(){
		GameObject.FindWithTag("Player").transform.position = new Vector3(0,1f,0);
		allscore -= 100f;
		if (allscore <= 0) {
			allscore = 0;
		}
		uiCtrl.setAllPoint ((int)allscore);
	}

}
