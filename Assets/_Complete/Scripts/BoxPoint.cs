﻿using UnityEngine;
using System.Collections;

public class BoxPoint : MonoBehaviour {

	public enum PointRole{
		Departure,Arrival,Neutral
	}

	PointRole pointRole;
	ParticleSystem myParticle;
	SpriteRenderer mySprite;
	Color defaultColor;
	Color particleDefaultColor;
	Color departureColor = Color.green;
	Color arrivalColor = Color.red;

	// Use this for initialization
	void Awake () {;

		myParticle = gameObject.GetComponent<ParticleSystem> ();
		particleDefaultColor = myParticle.startColor;
		mySprite = transform.FindChild ("GameObject").gameObject.GetComponent<SpriteRenderer> ();
		defaultColor = mySprite.color;
		pointRole = PointRole.Neutral;
	
	}
	
	// Update is called once per frame
	void Update () {

		switch (pointRole) {
		case PointRole.Arrival:
			mySprite.color = arrivalColor;
			myParticle.startColor = arrivalColor;
			break;
		case PointRole.Departure:
			mySprite.color = departureColor;
			myParticle.startColor = departureColor;
			break;
		case PointRole.Neutral:
			mySprite.color = defaultColor;
			myParticle.startColor = particleDefaultColor;
			break;
		}
	
	}

	public void setPointRole(PointRole p){
		pointRole = p;
	}

	public PointRole getPointRole(){
		return pointRole;
	}
}
