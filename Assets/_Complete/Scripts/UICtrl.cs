﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UICtrl : MonoBehaviour {

	[SerializeField] Text allPointText;
	[SerializeField] Text boxPointText; //= new Text[2];
	[SerializeField] Text timeText;
	[SerializeField] GameObject uiFinish;

	// Use this for initialization
	void Start () {
		allPointText.text = "ALL:0";
		boxPointText.text = "BOX:0";	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setBoxPoint(int point){
		boxPointText.text = "BOX:" + point;
	}

	public void setAllPoint(int point){
		allPointText.text = "ALL:" + point;	
	}

	public void setTime(float time){
		timeText.text = "TIME:" + (int)time;
	}
	
	public void setUIFinish(){
		uiFinish.SetActive (true);
	}
}
