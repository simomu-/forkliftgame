﻿using UnityEngine;
using System.Collections;


public class BoxGenerator : MonoBehaviour {
		
	[SerializeField] Transform[] boxGenerateTrans = new Transform[2];
	[SerializeField] GameObject boxPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider _other){

		if (_other.gameObject.tag == "Respawn") {
			BoxPoint point = _other.gameObject.GetComponent<BoxPoint>();
			switch(point.getPointRole()){
			case BoxPoint.PointRole.Departure:
				int boxNum = (int)Random.Range(0,boxGenerateTrans.Length);
				GameCtrl.BoxNum = boxNum + 1;
				for(int i = 0; i <= boxNum; i++){
					Instantiate(boxPrefab,boxGenerateTrans[i].position,boxGenerateTrans[i].rotation);
				}
				point.setPointRole(BoxPoint.PointRole.Neutral);
				break;
			case BoxPoint.PointRole.Arrival:
				break;
			}

		}

	}


}
