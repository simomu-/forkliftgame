﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleCtrl : MonoBehaviour {

	[SerializeField] Text highScoreText;

	FadeManager fadeMngr;
	int highScore = 0;

	void Awake(){
		fadeMngr = GameObject.Find ("FadeManager").GetComponent<FadeManager> ();
		highScore = PlayerPrefs.GetInt ("HighScore");
		highScoreText.text = "High Score:" + highScore;
	}

	void Update(){
	}

	public void gameStart(){
		fadeMngr.LoadLevel ("GameMain",2.0f);
	}

	public void gameEnd(){
		Application.Quit ();
	}

	public void resetHighScore(){
		PlayerPrefs.DeleteAll ();
		highScore = 0;
		highScoreText.text = "High Score:" + highScore;
	}

}
